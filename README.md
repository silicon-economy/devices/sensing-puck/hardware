# Sensing Puck Hardware

PCB Gerber Files, Enclosure and BOM for two Hardware Versions:

1. Temperature and Humidity: SHT3x
2. Time of Flight: VL53L1

## Documentation

[Documentation Version 1.0.1](https://git.openlogisticsfoundation.org/silicon-economy/devices/sensing-puck/documentation/-/tree/v1.0.1)

**The Documentation is for the version with the temperature and humidity sensor SHT3x and not for the Version with the ToF sensor VL53L1. Many contents are nevertheless also valid for the ToF Version**

## License

Licensed under the Open Logistics Foundation License 1.3.

## Contact Information

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
